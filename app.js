angular.module('app', ['mgo-angular-wizard', 'ngAnimate'])
	.controller('AppCtrl', ['$scope', 'WizardHandler', function($scope, WizardHandler){

		var vm = this;
		var summary = {};

		vm.addSummaryStep = false;

		vm.mainTitle = '';
		vm.intro = '';
		vm.introButton = '';
		vm.questionsCount = 0;

		///estructura con las preguntas y respuestas.
		if(questionaireData.questions){
			vm.questions = questionaireData.questions;
			vm.questionsCount = vm.questions.length;///numero total de preguntas		
			vm.addSummaryStep = true; ///habilito el step del summary, toca despues de traer las preguntas, por que si no, quedaba de segundo ¬¬
		}

		if(questionaireData.summary){
			summary = questionaireData.summary;
		}

		if(questionaireData.config){
			vm.mainTitle = questionaireData.config.mainTitle;
			vm.intro = questionaireData.config.intro;
			vm.introButton = questionaireData.config.introButton;
		}
			

		

		vm.answerQuestion = function(answer, question){

			///Si la respuesta es correcta, incremento el contador de respuestas correctas
			if(answer.correct)
				vm.questionsAnsweredCorrectly++;
			else
				vm.questionsAnsweredIncorrectlyTitles.push(question.title)

			///paso a la siguiente pregunta
			WizardHandler.wizard().next();
			
		}

		vm.getResultText = function(n){
			var text = '';
			var keys = Object.keys(summary);

			for(key in summary){
				var value = summary[key];
				if(vm.questionsAnsweredCorrectly <= parseInt(key)){
					text = value;
					break;
				}
			}
			
			return text;
		}

		vm.reset = function(){
			init();
			WizardHandler.wizard().reset();
		}

		function init(){
			vm.questionsAnsweredCorrectly = 0;///contador respuestas correctas
			vm.questionsAnsweredIncorrectlyTitles = [];
		}

		init();
	}])