var questionaireData = {
	"questions": [
			{
				"title": "¿Tu proyecto de vida es compatible con el de tu pareja?",
				"body": "",
				"answers" : [
					{
						"value": 1,
						"text": "No se, no conozco su proyecto de vida a fondo"
					},
					{
						"value": 2,
						"text": "Es totalmente opuesto al mío"
					},
					{
						"value": 3,
						"text": "Sí, es tan compatible que eso fue lo que en un principio nos unió",
						"correct": true
					}
				]
			},
			{
				"title": "¿Cómo se ven en el futuro?",
				"body": "",
				"answers" : [
					{
						"value": 1,
						"text": "No sabemos, solo vivimos el presente"
					},
					{
						"value": 2,
						"text": "Depende de cómo vaya la relación, todo puede pasar"
					},
					{
						"value": 3,
						"text": "Compartiendo juntos nuestra vejez",
						"correct": true
					}
				]
			},
			{
				"title": "¿Cuántos hijos planean tener?",
				"body": "Aqui podemos poner una explicacion o algo",
				"answers" : [
					{
						"value": 1,
						"text": "Todavía no nos ponemos de acuerdo en ese aspecto",
						"correct": true
					},
					{
						"value": 2,
						"text": "Yo no quiero hijos, pero aún no se lo he dicho"
					},
					{
						"value": 3,
						"text": "No queremos hijos, pero seguro adoptaremos una mascota"					
					}
				]
			},
			{
				"title": "¿Tu proyecto de vida es compatible con el de tu pareja? 2",
				"body": "",
				"answers" : [
					{
						"value": 1,
						"text": "No se, no conozco su proyecto de vida a fondo"
					},
					{
						"value": 2,
						"text": "Es totalmente opuesto al mío"
					},
					{
						"value": 3,
						"text": "Sí, es tan compatible que eso fue lo que en un principio nos unió",
						"correct": true
					}
				]
			},
			{
				"title": "¿Cómo se ven en el futuro? 2",
				"body": "",
				"answers" : [
					{
						"value": 1,
						"text": "No sabemos, solo vivimos el presente"
					},
					{
						"value": 2,
						"text": "Depende de cómo vaya la relación, todo puede pasar"
					},
					{
						"value": 3,
						"text": "Compartiendo juntos nuestra vejez",
						"correct": true
					}
				]
			},
			{
				"title": "¿Cuántos hijos planean tener? 2",
				"body": "Aqui podemos poner una explicacion o algo",
				"answers" : [
					{
						"value": 1,
						"text": "Todavía no nos ponemos de acuerdo en ese aspecto",
						"correct": true
					},
					{
						"value": 2,
						"text": "Yo no quiero hijos, pero aún no se lo he dicho"
					},
					{
						"value": 3,
						"text": "No queremos hijos, pero seguro adoptaremos una mascota"					
					}
				]
			},
			{
				"title": "¿Cuántos hijos planean tener? 3",
				"body": "Aqui podemos poner una explicacion o algo",
				"answers" : [
					{
						"value": 1,
						"text": "Todavía no nos ponemos de acuerdo en ese aspecto",
						"correct": true
					},
					{
						"value": 2,
						"text": "Yo no quiero hijos, pero aún no se lo he dicho"
					},
					{
						"value": 3,
						"text": "No queremos hijos, pero seguro adoptaremos una mascota"					
					}
				]
			}
		],

	"summary":{
		"2" : "Mensaje malo, (entre 0 y 2 correctas)",
		"4" : "Mensaje regular, (entre 3 y 4 correctas)",
		"7" : "Mensaje excelente, (entre 5 y 7 correctas)"
	},

	"config" : {
		"mainTitle" : "Text de Compatibilidad",
		"intro" : "Conocer muy bien los gustos, fortalezas, debilidades y actitudes frente a diversos aspectos de la vida de tu pareja, es fundamental para asegurar una convivencia armoniosa y duradera. Conoce aquí qué tan compatibles son tu amor y tu.",
		"introButton" : "Comenzar"
	}
}